#include <iostream>
//#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

int KO = 8, KK = 8, MU = 8, KL = 8, IN = 8, CH = 8, FF = 8, GE = 8;

void menu() {
    char starting = ' ';

    cout << "Wilkommen bei meinem kleinen Text-Advenure\n";
    cout << "Drücke J zum starten!: ";
    cin >> starting;

    if (starting == 'J' || starting == 'j') {
        system("clear");
    } else {
        cout << "Schade wohl keine Lust?\n";
    }
}

int roll_D20() {
    srand((unsigned) time(0));
    int randomNumber;
    for (int index = 0; index < 1; index++) {
        randomNumber = (rand() % 20) + 1;
    }
    return randomNumber;

}

//Ermittlung des Konstitutions Wertes
void consti() {
    //Konstitution!
    cout
            << "Du spürst den weichen Waldboden unter dir. Du schaust in die Baumkronen wo sich das Licht der Sonne bricht.\n"
               "Du versuchst dich aufzurichten, jedoch fühlst du dich noch etwas angeschlagen: Würfel einen W20! ";
    cin.ignore();
    KO = KO + roll_D20();
    if (KO > 17)
    {
        KO = 17;
    }
    cout << KO;
    cin.ignore();
    if (KO >= 14) {
        cout << "Wer ist denn hier angeschlagen?\nDu streckst und räkelst dich und stehst langsam auf!";
        cin.ignore();
    } else {
        cout << "Du merkst wie schwer es dir fällt aufzustehen und brauchst sehr lange um dich aufzuraffen!";
        cin.ignore();
    }
}

//Consti mit enthalten erster Text!
void intro()
{
    cout << "Tipp! Drücke eine Taste um weiterzukommen!\n";
    cin.ignore();
    cout << "Du hörst Vogelgezwitscher, Geräusche des Waldes umgeben dich!";
    cin.ignore();
    cout << "Langsam öffnest du deine Augen";
    cin.ignore();
    cout << "...";
    cin.ignore();
    cout << "...";
    cin.ignore();
    cout << "...";
    cin.ignore();
    consti();
    cout << "Nachdem du wach geworden und aufgestanden bist fühlst du dich immer noch durcheinander!\n";
    cout << "Du bist in einem dunklen Wald und es fühlt sich an wie ein Traum!";
    cin.ignore();
    cout << "Die Sonne geht gerade auf!";
    cin.ignore();
    cout << "Um dich herum, in jede Richtung in der du blickst, siehst du Bäume! Nun du bist ja auch in einem Wald.";
    cin.ignore();
    cout << "Also wohin gehst du? ";
}

//Ermittlung der Intuition
void initative()
{
        char stamp = ' ';
        cout << "Oh! Was ist das denn, eine Riesenratte?\n";
        cin.ignore();
        cout << "Was willst du tun? Sie zertrampeln oder Weglaufen?(A:Zertrampeln; B:Weglaufen): \n";
        cin >> stamp;
        if (stamp == 'a' || stamp == 'A')
        {
            cout << "Langsam hebst du dein Bein an ... und!";
            cin.ignore();
            cout << "Würfel einen W20: ";
            cin.ignore();
            IN = IN + roll_D20();
            if (IN > 17)
            {
                IN = 17;
            }
            cout << IN;
            cin.ignore();
            if (IN >= 15) {
                cout << "Wer sagt denn das, dass eine Ratte sei. Es fühlte sich an wie eine Maus! Haaaa!\n";
                cin.ignore();
            }
            else
            {
                cout
                        << "Uii wer hätte denn gedacht das diese Ratte gefühlt so groß wie ein ausgewachsener Feldhase aussehen würde?\n";
            }
        }
        else if (stamp == 'b' || stamp == 'B')
        {
            cout << "Du kleiner Angsthase!\nDu kehrst zurück!";
            cin.ignore();
        }
        else
        {
            cout << "Ach komm schon!";
            cin.ignore();
        }
}

//Ermittlung der Körperkraft
void power()
{
    char decision = ' ';
    cout << "Huch vor dir Liegt ein Großer Baumstamm!\n";
    cin.ignore();
    cout << "Willst du versuchen ihn aus dem Weg zu räumen? (J/N): ";
    cin >> decision;

    if (decision == 'J' || decision == 'j')
        {
            cout << "Du greifst mit beiden Händen an den Baumstamm und versuchst ihn zu bewegen.\n";
            cin.ignore();
            cout << "Werf einen W20: ";
            cin.ignore();
            KK = KK + roll_D20();
            if (KK > 17)
            {
                KK = 17;
            }
            cout << KK;
            cin.ignore();

            if (KK >= 14)
            {
                cout << "Deine Kraft geriet zurück in deinen Körper du schiebst den Baumstamm ohne große Mühe von dir weg!";
                cin.ignore();
            }
            else
            {
                cout << "Meine güte wie schwer kann ein Baumstamm schon sein. Nach mehreren Versuchen gelingt es dir endlich!";
                cin.ignore();
            }
        }

        if (decision == 'N' || decision == 'n')
        {
            cout << "Was ein Baumstamm?\nNe das geht garnicht!\nDu gehst wieder zurück";
            cin.ignore();
        }
}

//Ermittlung der Mut
void courage()
{
    cout << "Was wie?...\n";
    cin.ignore();
    cout << "Dir wird ganz komisch! Du hörst ein gespenstisches Flüstern!\n";
    cin.ignore();
    cout << "Wirf einen W20: ";
    MU = MU + roll_D20();
    cin.ignore();
    if(MU > 17)
    {
        MU = 17;
    }
    cout << MU;
    cin.ignore();
    if (MU >= 13)
    {
        cout << "Ach! Kein Geist kann mir was!";
        cin.ignore();
        cout << "Aber irgendwie verstehe ich nicht was sie sagen.";
        cin.ignore();
    }
    else
    {
        cout << "Hilfe!";
        cin.ignore();
        cout << "Dich überkommt die Angst!";
        cin.ignore();
        cout << "Du kehrst zurück!";
        cin.ignore();
    }

}

//Ermittlung der Klugheit
void wisdom()
{
    cout << "Vor dir steht eine Riesige Eiche!\n";
    cin.ignore();
    cout << "Ein Zettel hängt dran, mit der Aufschrift: \n";
    cin.ignore();
    cout << "Du bekommst 4 Birnen und gibst eine ab.\n";
    cout << "Wieviel Birnen hast du noch?\n"; cout << "...\n";
    cout << "Wirf einen W20: ";
    cin.ignore();
    KL = KL + roll_D20();
    if(KL > 17)
    {
        KL = 17;
    }
    cout << KL;
    cin.ignore();
    if(KL >= 11)
    {
        cout << "Natürlich drei, was hast du denn gedacht?";
        cin.ignore();
    }
    else
    {
        cout << "Du bist dir so sicher, dass es 2 sein müssen!";
        cin.ignore();
    }
}

//Funktion zur Abfrage der Richtung und Ausgabe von einem Kurzen Text in welche Richtung der Spieler gegangen ist
char direction()
{
    char direction = ' ';
    bool allowedDirection = false;

    cout << "Bewege dich mit WASD um in eine Richtung zu gehen!\n";
    cin >> direction;

    while(!allowedDirection)
    {
        switch (direction)
        {
            case 'w':
                cout << "Du gehst Geradeaus weiter!\n";
                power();
                allowedDirection = true;
                break;
            case 'a':
                cout << "Du gehst nach Links!\n";
                initative();
                allowedDirection = true;
                break;
            case 'd':
                cout << "Du gehst nach Rechts!\n";
                courage();
                allowedDirection = true;
                break;
            case 's':
                cout << "Du gehst zurück!\n";
                wisdom();
                allowedDirection = true;
                break;
            default:
                cout << "Du musst dich schon entscheiden!\n";
                break;
        }
    }
    return direction;
}

int main() {

    //menu();
    //Das eigentliche Spiel startet
    intro();
    direction();

    return 0;
}
